module.exports = {
    auth: require('./auth').auth,
    uploadFile: require('./upload_file').uploadFile
}