module.exports = {
    job: require('./job'),
    company: require('./company'),
    user: require('./user'),
    apply_job: require('./apply_job'),
    notify: require('./notification'),
}
